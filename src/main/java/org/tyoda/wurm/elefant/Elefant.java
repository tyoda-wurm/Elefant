/*
Elefant mod for Wurm Unlimited
Copyright (C) 2024 Tyoda

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package org.tyoda.wurm.elefant;

import org.gotti.wurmunlimited.modloader.interfaces.Configurable;
import org.gotti.wurmunlimited.modloader.interfaces.ItemTemplatesCreatedListener;
import org.gotti.wurmunlimited.modloader.interfaces.WurmServerMod;
import org.gotti.wurmunlimited.modsupport.items.ModItems;
import org.tyoda.wurm.elefant.items.WoodenElephant;

import java.util.Properties;
import java.util.logging.Logger;

public class Elefant implements WurmServerMod, ItemTemplatesCreatedListener, Configurable {
    public static Logger logger = Logger.getLogger(Elefant.class.getName());
    public static boolean craftWithPlanks = true;
    public boolean craftWithLog = true;
    public int craftHowManyPlanks = 10;
    private static Elefant instance;
    public Elefant(){
        instance = this;
    }

    @Override
    public void configure(Properties p) {
        craftWithPlanks = Boolean.parseBoolean(p.getProperty("craftWithPlanks", String.valueOf(craftWithPlanks))); // default: "false"
        craftHowManyPlanks = Integer.parseInt(p.getProperty("craftHowManyPlanks", String.valueOf(craftHowManyPlanks)));
        craftWithLog = Boolean.parseBoolean(p.getProperty("craftWithLog", String.valueOf(craftWithLog)));
    }

    @Override
    public void onItemTemplatesCreated() {
        ModItems.init();
        WoodenElephant.onItemTemplatesCreated();
    }

    public static Elefant getInstance(){
        return instance;
    }

    @Override
    public String getVersion() {
        return "1.0";
    }
}
